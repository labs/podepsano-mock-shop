var xml_parts = new Array();
xml_parts[1] = "<?xml version='1.0' encoding='UTF-8'?>\n<request_bundle xmlns='https://www.podepsano.cz/'>\n    <provider_id>";
xml_parts[2] = "</provider_id>\n    <provider_request_id>";
xml_parts[3] = "</provider_request_id>\n    <provider_return_url>";
xml_parts[4] = "</provider_return_url>\n    <provider_requested_id_attributes>\n";
xml_parts[5] = "    </provider_requested_id_attributes>\n    <request>\n        <subject>";
xml_parts[6] = "</subject>\n        <content type='text/plain'>\n";
xml_parts[7] = "\n        </content>\n    </request>\n</request_bundle>";

function fillXML() {
    for (var i = 1; i < xml_parts.length; i++) {
        var xml_active = document.getElementById("xml_part"+i);
        xml_active.textContent = xml_parts[i];
    }
}
fillXML();

var active_field = null;

function fieldFocus(elem) {
    //console.log("focus");
    //console.log(elem.name);
    active_field = elem;
}

function fieldBlur(elem) {
    //console.log("end");
    active_field = null;
    Deactive(elem);
}

function xml_esc(str) {
    return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

function Active() {
    if (active_field) {
        //console.log(active_field.name);
        var xml_active = document.getElementById("xml_"+active_field.name);
        //xml_active.style.color = "#f00";
        //xml_active.style.fontWeight = "bold";
        xml_active.style.background = "#f00";
        xml_active.textContent = xml_esc(active_field.value);
    }
}

function Deactive(elem) {
    var xml_active = document.getElementById("xml_"+elem.name);
    //xml_active.style.color = "#000";
    //xml_active.style.fontWeight = "normal";
    xml_active.style.background = "#fff";
}

function fieldCheckbox() {
    var xml_active = document.getElementById("xml_checkbox");
    xml_active.textContent = "";
    xml_active.style.color = "#f00";

    var element;
    for (var i = 1; element = document.getElementById("check"+i); i++) {
        if (element.checked) {
            //console.log(element.name);
            //&lt;req_id_attr&gt;http://specs.nic.cz/attr/phone/main&lt;/req_id_attr&gt;
            //var text = "        &lt;req_id_attr&gt;" + element.name + "&lt;/req_id_attr&gt;\n";
            var text = "        <req_id_attr>" + element.name + "</req_id_attr>\n";
            xml_active.appendChild(document.createTextNode(text));
        }
    }
}

function send() {
    // generating XML
    var spans = document.getElementsByTagName('span');
    var together = ""
                   
    for (var i = 0; i < spans.length; i++) {
        /*
        if (spans[i].id.substring(4,7) == "var") {
            // escape characters
            var esc = spans[i].textContent
            together += esc.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        } else {
            together += spans[i].textContent;
        }
        */
        together += spans[i].textContent;
    }
    
    // filling hidden element
    var xmldata = document.getElementById("xmldata");
    xmldata.value = together;
    
    // sending form
    document.getElementById('sending_form').submit();
}

function mainLoop() {
    Active();
    setTimeout(mainLoop, 10);
}
mainLoop();

