==============================================
How to install and run Mockshop Podepsano.cz server
==============================================

--------------------------
1. Install prerequisites
--------------------------

* Python 2.6+

Built in packages:

    aptitude install python-django libxml2-dev libxmlsec1-dev python-yaml python-setuptools python-dev python-lxml

-----------------------
2. Configure settings
-----------------------

Copy settings_local.py.example into settings_local.py and fill in the values that are specific for this installation - edit paths 'BASE_SHARE_DIR', 'MEDIA_ROOT' and 'NOTARY_CERTIFICATE_FILE'.


--------------------------
3. Test your setup
--------------------------

Run a test server like this:

    python notary/manage.py runserver 0.0.0.0:8000

and test that you can connect to the server at 0.0.0.0:8000/podepsano/mock/start and that the site looks as it should.
