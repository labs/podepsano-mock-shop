import xml.dom.minidom as dom
from xml.sax.saxutils import escape

# built-in modules
import logging

# third party imports
import libxml2
import xmlsec

import yaml
import os

log = logging.getLogger(__name__)

# SIGNATURE VALIDATION RESULT CONSTANTS
SIGNATURE_VALID = 0
SIGNATURE_INVALID = 1
SIGNATURE_VERIFY_FAILED = 2
SIGNATURE_ROOT_ELEMENT_NOT_FOUND = 3
SIGNATURE_XML_PARSING_FAILED = 4
SIGNATURE_CANNOT_CREATE_CONTEXT = 5

def create_xml(company_name, req_id, return_url, requested_attrs, subject, content):
    """
    Returns string of generated XML.
    """
    xml_parts = []
    xml_parts.append("<?xml version='1.0' encoding='UTF-8'?>\n<request_bundle xmlns='https://www.podepsano.cz/'>\n    <provider_id>")
    xml_parts.append("</provider_id>\n    <provider_request_id>")
    xml_parts.append("</provider_request_id>\n    <provider_return_url>")
    xml_parts.append("</provider_return_url>\n    <provider_requested_id_attributes>\n")
    xml_parts.append("    </provider_requested_id_attributes>\n    <request>\n        <subject>")
    xml_parts.append("</subject>\n        <content type='text/plain'>\n")
    xml_parts.append("\n        </content>\n    </request>\n</request_bundle>")
    
    requested_attrs_result = ""
    for one in requested_attrs:
        requested_attrs_result += one + "\n"
    
    result = xml_parts[0] + escape(company_name) + xml_parts[1] + escape(req_id) + xml_parts[2] + escape(return_url) + xml_parts[3] + escape(requested_attrs_result) + xml_parts[4] + escape(subject) + xml_parts[5] + escape(content) + xml_parts[6]
    return result

def parse_xml(xml_data):
    """
    Parse given xml_data.
    """
    try:
        parsed_data = dom.parseString(xml_data)
    except Exception as exc:
        return False

    error = False
    result = {}

    requested_attrs = []
    for one in parsed_data.getElementsByTagName("id_attr"):
        requested_attrs.append([ one.getAttribute("name"), one.childNodes[0].nodeValue ])
    result['requested_attrs'] = requested_attrs

    for one in parsed_data.getElementsByTagName("confirmation"):
        status = one.getAttribute("status")
        if status == "confirmed":
            result['confirmed'] = True
        elif status == "rejected":
            result['confirmed'] = False
        else:
            error = True
            
    for conf in parsed_data.getElementsByTagName("err_code"):
        error = True

    for one in parsed_data.getElementsByTagName("subject"):
        result['subject'] = one.childNodes[0].nodeValue
    for one in parsed_data.getElementsByTagName("content"):
        result['content'] = one.childNodes[0].nodeValue
    
    if error:
        return False
        
    return result
    
def translate_attr(yaml_file, wanted, language_filter="cs"):
    """
    Returns name of wanted attribute from yaml dictionary.
    """
    try:
        f = open(yaml_file,'r')
    except:
        log.error("Error: Can't open" + yaml_file + ".\n")
        return wanted

    data_yaml = yaml.load(f)
    f.close()

    wanted_attribute = 0

    # looking for wanted_attribute
    for one in data_yaml:
        if len(one['fields'].keys()) == 2:
            if one['fields']['url'] == wanted:
                wanted_attribute = one['fields']['attribute']

    # wanted_attribute not found
    if not wanted_attribute:
        return wanted


    # looking for name
    for one in data_yaml:
        if len(one['fields'].keys()) == 3:
            if one['fields']['master'] == wanted_attribute:
                # language filter
                if one['fields']['language_code'] == language_filter:
                    return one['fields']['name']
                    
    # no match
    return wanted

def init():    
    # Init libxml library
    #libxml2.initParser()
    #libxml2.substituteEntitiesDefault(1)

    # Init xmlsec library
    if xmlsec.init() < 0:
        log.error("Error: xmlsec initialization failed.")
        return -1
    
    # Check loaded library version
    if xmlsec.checkVersion() != 1:
        log.error("Error: loaded xmlsec library version is not compatible.\n")
        return -1

    # Init crypto library
    if xmlsec.cryptoAppInit(None) < 0:
        log.error("Error: crypto initialization failed.")
    
    # Init xmlsec-crypto library
    if xmlsec.cryptoInit() < 0:
        log.error("Error: xmlsec-crypto initialization failed.")

def destroy():
    # Shutdown xmlsec-crypto library
    xmlsec.cryptoShutdown()

    # Shutdown crypto library
    xmlsec.cryptoAppShutdown()

    # Shutdown xmlsec library
    xmlsec.shutdown()

    # Shutdown LibXML2
    #libxml2.cleanupParser()
    # cannot cleanup parser, because lxml also uses libxml and we get some segfaults....
    pass

def get_keymanager(certificate_files):    
    mngr = xmlsec.KeysMngr()
    if xmlsec.cryptoAppDefaultKeysMngrInit(mngr) < 0:
        print "Error: failed to initialize keys manager."
        mngr.destroy()
        return False
        
    for cert in certificate_files:
        if mngr.certLoad(cert, xmlsec.KeyDataFormatPem, xmlsec.KeyDataTypeTrusted) < 0:
            print "Error: failed to load pem certificate from \"%s\"", file
            mngr.destroy()
            return None

    return mngr

def verify_xml(certificate_path, xml_data):
    """
    Returns an integer from the SIGNATURE_* set described above in this module.
    Anything other than SIGNATURE_VALID means the signature is not OK.
    SIGNATURE_INVALID means the signature does not match, the rest is some kind
    of error state.
    """
    
    certificate_files = [os.path.join(certificate_path, f) for f in os.listdir(certificate_path)]
    
    init()
    
    mngr = get_keymanager(certificate_files)
    if not mngr:
        raise Exception("KeyManager init error")
    
    doc = libxml2.parseMemory(xml_data, len(xml_data))
    if doc is None or doc.getRootElement() is None:
        log.error("Error: unable to parse file")
        cleanup(doc)
        destroy()
        return SIGNATURE_XML_PARSING_FAILED

    # Find start node
    node = xmlsec.findNode(doc.getRootElement(),
                           xmlsec.NodeSignature, xmlsec.DSigNs)
    if node is None:
        log.error("Error: start node not found")
        cleanup(doc)
        destroy()
        return SIGNATURE_ROOT_ELEMENT_NOT_FOUND
    # Create signature context
    dsig_ctx = xmlsec.DSigCtx(mngr)
    if dsig_ctx is None:
        log.error("Error: failed to create signature context")
        cleanup(doc)
        destroy()
        return SIGNATURE_CANNOT_CREATE_CONTEXT

    # Verify signature
    if dsig_ctx.verify(node) < 0:
        log.error("Error: signature verify")
        cleanup(doc, dsig_ctx)
        destroy()
        return SIGNATURE_VERIFY_FAILED

    # Print verification result to stdout
    if dsig_ctx.status == xmlsec.DSigStatusSucceeded:
        log.debug("Signature is OK")
        res = SIGNATURE_VALID
    else:
        log.debug("Signature is INVALID")
        res = SIGNATURE_INVALID

    # Success
    cleanup(doc, dsig_ctx, 1)
    destroy()
    return res

def cleanup(doc=None, dsig_ctx=None, res=-1):
    if dsig_ctx is not None:
        dsig_ctx.destroy()
    if doc is not None:
        doc.freeDoc()
    return res
