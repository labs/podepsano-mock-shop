from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('notary.mockprovider.views',
    (r'^start/$', 'start_order'),
    (r'^generator/$', 'generator'),
    (r'^finish/$', 'finish_order'),    
)
