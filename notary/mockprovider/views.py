from django.shortcuts import render_to_response
from django.conf import settings

from notary import util
import xml.dom.minidom as dom

import podepsano

def start_order(request):
    notar_start = "https://www.podepsano.cz/sign"

    callback = util.getViewURL(request, finish_order)
    return render_to_response('start.html', {'start': notar_start, 'finish': callback, 'links': links})

def generator(request):
    notar_start = "https://www.podepsano.cz/sign"
    
    callback = util.getViewURL(request, finish_order)
    return render_to_response('generator.html', {'start': notar_start, 'finish': callback, 'links': links})

def finish_order(request):
    try:
        xml_data = request.POST["signed_xml"]
    except:
        return render_to_response('finish.html', {'links': links})
   
    # check signature
    sig_ok = None
    res = podepsano.verify_xml(settings.NOTARY_CERTIFICATE_PATH, xml_data)
    if res == podepsano.SIGNATURE_VALID:
        sig_ok = True
    elif res == podepsano.SIGNATURE_INVALID:
        sig_ok = False
    else:
        sig_ok = "Error"

    # parse xml
    data = podepsano.parse_xml(xml_data)
    if data:
        
        # check content - attributes to be checked:
        #data["subject"]
        #data["content"]
        
        # translating requested_attrs
        for one in data["requested_attrs"]:
            one[0] = podepsano.translate_attr(settings.BASE_SHARE_DIR+'attribute.yaml' ,one[0])
        
        
        return render_to_response('finish.html',
                                          {'confirmed': data["confirmed"], "error": False, 'requested_attrs': data["requested_attrs"], 'links': links, 'sig_ok': sig_ok})
                
    else:
        return render_to_response('finish.html',
                                  {'confirmed': False, "error": True, 'links': links, 'sig_ok': sig_ok})


MOCK_URL = "mock/"
links = {
        'media': "/" + settings.BASE_URL + "media/",
        'start': "/" + settings.BASE_URL + MOCK_URL + "start",
        'generator': "/" + settings.BASE_URL + MOCK_URL + "generator",
        'finish': "/" + settings.BASE_URL + MOCK_URL + "finish",
        }